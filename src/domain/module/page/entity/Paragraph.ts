/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Paragraph
 */

export class Paragraph {
    text: string;
    order = -1;

    constructor(text: string, order = -1) {
        this.text = text;
        this.order = order;
    }

    public get rows() {
        return this.text.split("\n").length;
    }
}
