/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Progress
 */
export class Progress {
    value: number;

    constructor(value: number) {
        this.value = value;
    }
}
