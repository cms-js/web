/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Image
 */
export class Image {
    url: string;

    constructor(url: string) {
        this.url = url;
    }
}
