/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Textarea
 */

export class Textarea {
    label: string;

    constructor(label: string) {
        this.label = label;
    }
}
