/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Content
 */
export class Header {
    text: string;

    constructor(text: string) {
        this.text = text;
    }
}
