/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Field
 */

export class Field {
    label: string;

    constructor(label: string) {
        this.label = label;
    }
}
