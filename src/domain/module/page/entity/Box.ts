/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Box
 */
import { Header } from "./Header";
import { Paragraph } from "./Paragraph";
import { Icon } from "./Icon";
import { Field } from "./Field";
import { Textarea } from "./Textarea";
import { Image } from "./Image";
import { Type } from "class-transformer";

export class Box {
    name: string;

    @Type(() => Header)
    headers: Header[] = [];

    @Type(() => Paragraph)
    paragraphs: Paragraph[] = [];

    @Type(() => Icon)
    icons: Icon[] = [];

    @Type(() => Field)
    fields: Field[] = [];

    @Type(() => Textarea)
    textareas: Textarea[] = [];

    @Type(() => Image)
    images: Image[] = [];

    order = -1;

    constructor(name: string, order = -1) {
        this.name = name;
        this.order = order;
    }

}
