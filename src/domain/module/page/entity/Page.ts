/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Page
 */
import { Box } from "./Box";
import { Type } from "class-transformer";

export class Page {
    id!: string;

    name: string;

    @Type(() => Box)
    boxes: Box[] = [];

    constructor(name: string, ...boxes: Box[]) {
        this.name = name;
        this.boxes = boxes;
    }

    public rename(name: string): this {
        this.name = name;
        return this;
    }

    public getBoxes(): Record<string, Box> {
        return this.boxes.reduce((accum, box) => {
            accum[box.name] = box;
            return accum;
        }, {} as Record<string, Box>);
    }

    public getBox(name: string): Box {
        const box = this.boxes.find(item => item.name === name);
        if(!box) {
            throw new Error("Box not found.");
        }
        return box;
    }
}
