/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Icon
 */
export class Icon {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

}
